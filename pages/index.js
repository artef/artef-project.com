import React from 'react';
import ArtefHead from '../components/Head';
import Layout from '../components/Layout';
import Main from '../components/Main';

const Home = () => (
  <Layout>
    <ArtefHead />

    <Main />

    <style global jsx>{`
      body {
        font-family: Helvetica;
        font-weight: 200;
        color: #333;
      }

      h1,
      h2,
      h3,
      h4,
      h5,
      p {
        font-weight: 200;
      }

      /* Smartphones (portrait and landscape) ----------- */
      @media only screen and (min-device-width: 320px) and (max-device-width: 480px) {
        body {
          font-size: 1em;
        }

        h1 {
          font-size: 1.5em;
        }
      }

      /* iPads (portrait and landscape) ----------- */
      @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
        body {
          font-size: 1.2em;
        }

        h1 {
          font-size: 2em;
        }
      }

      /* Desktops and laptops ----------- */
      @media only screen and (min-width: 1224px) {
        body {
          font-size: 1.2em;
        }

        h1 {
          font-size: 2em;
        }
      }
    `}</style>
  </Layout>
);

export default Home;
