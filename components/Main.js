import Logo from '../components/Logo';

const Main = () => (
  <div className="hero">
    <Logo />
    <h1>Under construction 🏗</h1>
    <style jsx>{`
      .hero {
        width: 100%;
        height: 90vh;
        display: flex;
        flex-direction: column;
        justify-content: center;
        text-align: center;
      }
    `}</style>
  </div>
);

export default Main;
