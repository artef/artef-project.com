const Logo = () => (
  <div>
    <img
      className="logo"
      src="/images/artef-logo-2742w.jpg"
      srcSet="
      /images/artef-logo-548w.jpg 1x,
      /images/artef-logo-1097w.jpg 2x,
      /images/artef-logo-1645w.jpg 3x,
      /images/artef-logo-2194w.jpg 4x,
      /images/artef-logo-2742w.jpg 5x
      "
      alt="Artef company logo"
    />
    <style jsx>{`
      .logo {
        max-width: 100%;
      }
    `}</style>
  </div>
);

export default Logo;
