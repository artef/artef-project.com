import Head from 'next/head';

const ArtefHead = () => (
  <Head>
    <title>Artef Project</title>

    <meta name="title" content="Artef Project" />
    <meta name="description" content="" />

    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://artef-project.com/" />
    <meta property="og:title" content="Artef Project" />
    <meta property="og:description" content="" />

    <meta property="twitter:url" content="https://artef-project.com/" />
    <meta property="twitter:title" content="Artef Project" />
    <meta property="twitter:description" content="" />

    <link rel="icon" href="/favicon.ico" />
  </Head>
);

export default ArtefHead;
