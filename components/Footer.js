const Footer = () => (
  <footer>
    <a href="mailto:artefproject@gmail.com?subject=Contact from website">
      Get in touch ✉️
    </a>
    <style jsx>{`
      footer {
        text-align: center;
      }

      a {
        text-decoration: none;
        color: #333;
        opacity: 0.6;
      }

      a:hover {
        opacity: 1;
      }
    `}</style>
  </footer>
);

export default Footer;
