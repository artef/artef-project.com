import ArtefHead from './Head';
import Footer from './Footer';

const Layout = props => (
  <div className="layout">
    <main>{props.children}</main>
    <Footer />
    <style jsx>{`
      .layout {
        display: grid;
        grid-template-areas: 'header' 'content' 'footer';
        grid-gap: 10px;
      }
    `}</style>
  </div>
);

export default Layout;
